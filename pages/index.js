import Head from 'next/head';

function Index() {
  return <div>
    <Head>
      <link rel="shortcut icon" href="/static/favicon.ico" />
      <title>Pepega</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
      <link rel="stylesheet" href="/static/main.css"/>
    </Head>
    <center>
      <img src="/static/pepega.png"/>
      <h1><a href="mailto:yep@pepega.ml">PEPEGA</a></h1>
      <h4><p style={{color:'grey'}}>yep@pepega.ml</p></h4>
    </center>
  </div>
}

export default Index;